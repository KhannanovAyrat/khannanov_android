package ru.khannanovayrat.myapplication;

import io.realm.RealmObject;

/**
 * Created by admin on 19.06.2015.
 */
public class JsonHelperClass extends RealmObject {

    private String id;
    private String name;

    public JsonHelperClass(){}

    public JsonHelperClass(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId(){
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(String id){
        id = id;
    }

    public void setName(String name){
        name = name;
    }

}
