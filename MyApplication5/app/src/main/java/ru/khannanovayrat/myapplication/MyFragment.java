package ru.khannanovayrat.myapplication;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MyFragment extends Fragment {

    public static String LOG_TAG = "my_log";
    ListView lv;
    DetailFragment myFragment;
    Realm realm;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View myFragmentView = inflater.inflate(R.layout.fragmentlayout,
                container, false);
        lv = (ListView) myFragmentView.findViewById(R.id.listView);
        /*String[] resultString = {"1", "2", "3"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, resultString);
        lv.setAdapter(adapter);*/

        ParseTask task = new ParseTask(getActivity());
        task.execute();
        realm = Realm.getInstance(getActivity());

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, final View itemClicked, int position, long id){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                myFragment = new DetailFragment();
                fragmentTransaction.add(R.id.myfragment, myFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TextView textFragment = (TextView) getView().findViewById(R.id.fragmenttext);
                        RealmQuery<JsonHelperClass> query = realm.where(JsonHelperClass.class);
                        RealmResults<JsonHelperClass> result;
                        query.equalTo("name",((TextView)itemClicked).getText().toString());
                        result = query.findAll();
                        textFragment.setText(result.get(0).getName());
                    }
                }, 100);

            }
        });
        return myFragmentView;
    }

    private class ParseTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        public List<String> list = new ArrayList<String>();
        public boolean isDone = false;
        Context context;

        public ParseTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://formatio.ru/api/v1/cities");

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        @Override
        protected void onPostExecute(String strJson){
            super.onPostExecute(strJson);

            Log.d(LOG_TAG, strJson);

            JSONObject dataJsonObj = null;
            String cityName = "";
            String cityId = "";
            String temp = "";

            try {
                dataJsonObj = new JSONObject(strJson);
                JSONArray cities = dataJsonObj.getJSONArray("cities");
                String[] citiesNames = new String[cities.length()];

                for (int i = 0; i < cities.length(); i++) {
                    JSONObject city = cities.getJSONObject(i);
                    cityName = city.getString("name");
                    cityId = city.getString("id");
                    citiesNames[i] = cityName;
                    Log.d("City ", city.getString("id") + " " + city.getString("name"));

                    realm.beginTransaction();
                    JsonHelperClass cityJson = realm.createObject(JsonHelperClass.class);
                    Log.d("City ", cityId + " " + cityName + " written");
                    cityJson.setId(cityId);
                    cityJson.setName(cityName);
                    realm.commitTransaction();

                    temp = cityName;
                    //Log.d(LOG_TAG, "�����: " + list.get(i));
                }


                RealmQuery<JsonHelperClass> query = realm.where(JsonHelperClass.class);
                RealmResults<JsonHelperClass> result = query.findAll();
                String[] resultString = new String[cities.length()];
                Log.d("", result.get(7).toString());
                for (int i = 0; i < cities.length(); i++){
                    resultString[i] = result.get(i+2).getName();
                    Log.d("City ", result.get(i+2).getId() + " " + result.get(i+2).getName());
                }

                //String[] resultString1 = {"1", "2", "3"};
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, resultString);
                lv.setAdapter(adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}